/*
 *即时通讯相关仓库
 */
export default {
    state: {
        //打开的聊天窗口数量
        dialogUsers: []
    },
    mutations: {
        //添加人员到聊天窗口中
        pushDialogUser(state, payload) {
            if (payload) {
                const obj = _.find(state.dialogUsers, (o) => {
                    return o.id === payload.id;
                });
                if (!obj) {
                    state.dialogUsers.push(payload);
                }
            }
        },
        //移除人员到聊天窗口中
        removeDialogUser(state, payload) {
            if (payload) {
                state.dialogUsers.splice(state.dialogUsers.indexOf(payload), 1);
            }
        },
        //清空人员聊天数量
        emptyDialogUser(state, payload) {
            state.dialogUsers = payload;
        }
    },
    actions: {}
}