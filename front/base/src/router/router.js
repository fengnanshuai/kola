import Vue from 'vue'
//1.引入 Router
import Router from 'vue-router'
// import main from '@/components/framework/AppMain'
// import UserSetting from '@/components/user/UserSetting'
// import MenuQuery from '@/components/menu/MenuQuery'
// import Login from '@/components/login/Login'
// import IViewDemo from '@/components/IViewDemo'
// import Index from '@/components/Index'
/**
 *解决因为路由版本导致的message: "Navigating to current location ("/homePage") is not allowed",警告的问题
 */
const originalPush = Router.prototype.push;
Router.prototype.push = function push(location) {
    return originalPush.call(this, location).catch(err => err)
};

//2.注册 Router 到 Vue
Vue.use(Router);
/**
 *1 . vue异步组件技术 ==== 异步加载
 * component: resolve => require(['@/components/home'],resolve)
 *2 . 组件懒加载方案二 路由懒加载(使用import)
 * 没有指定webpackChunkName，每个组件打包成一个js文件。
 * component: () => import('@/components/menu/MenuQuery')
 * 3 . webpack提供的require.ensure()
 * vue-router配置路由，使用webpack的require.ensure技术，也可以实现按需加载。
 * 这种情况下，多个路由指定相同的chunkName，会合并打包成一个js文件。
 * component: r => require.ensure([], () => r(require('@/components/home')), 'demo')
 */
//3.实例化 Router
export default new Router({
    //配置路由
    routes: [
        {
            path: '/',
            name: 'Login',
            meta: {title: 'Login', hideInMenu: true},
            component: () => import('@/components/container/main/Login')
        },
        {
            path: '/index',
            name: 'Index',
            meta: {title: 'Index', hideInMenu: true},
            component: () => import('@/components/container/main/Index'),
            children: [
                {
                    path: 'main',
                    name: 'main',
                    meta: {title: '首页', icon: 'ios-cafe'},
                    component: () => import('@/components/views/main/MainIndex')
                }, {
                    path: 'dict',
                    name: 'dict',
                    meta: {title: '字典', icon: 'ios-cafe'},
                    component: () => import('@/components/views/dict/DictManager')
                }, {
                    path: 'notice',
                    name: 'notice',
                    meta: {title: '通知中心', icon: 'ios-cafe'},
                    component: () => import('@/components/views/notice/NoticeManager')
                }, {
                    path: 'menus',
                    name: 'menus',
                    meta: {title: '菜单管理', icon: 'ios-cafe'},
                    component: () => import('@/components/views/menu/MenuManager')
                }, {
                    path: 'user',
                    name: 'user',
                    meta: {title: '用户管理', icon: 'ios-cafe'},
                    component: () => import('@/components/views/user/UserManager')
                }, {
                    path: 'user/group',
                    name: 'userGroup',
                    meta: {title: '用户组管理', icon: 'ios-cafe'},
                    component: () => import('@/components/views/user/UserGroupManager')
                }, {
                    path: 'role',
                    name: 'role',
                    meta: {title: '角色管理', icon: 'ios-cafe'},
                    component: () => import('@/components/views/role/RoleManager')
                }, {
                    path: 'permission',
                    name: 'permission',
                    meta: {title: '权限管理', icon: 'ios-cafe'},
                    component: () => import('@/components/views/permission/PermissionManager')
                },
                {
                    path: 'doList',
                    name: 'doList',
                    meta: {title: '我办理的', icon: 'ios-cafe'},
                    component: () => import('@/components/views/taskCenter/MyDone')
                },
                {
                    path: 'todoList',
                    name: 'todoList',
                    meta: {title: '我的待办', icon: 'ios-cafe'},
                    component: () => import('@/components/views/taskCenter/MyTodo')
                },
                {
                    path: 'transferList',
                    name: 'transferList',
                    meta: {title: '我交办的', icon: 'ios-cafe'},
                    component: () => import('@/components/views/taskCenter/MyTransfer')
                },
                {
                    path: 'startedList',
                    name: 'startedList',
                    meta: {title: '我发起的', icon: 'ios-cafe'},
                    component: () => import('@/components/views/taskCenter/MyStarted')
                },
                {
                    path: 'draftList',
                    name: 'draftList',
                    meta: {title: '我的草稿', icon: 'ios-cafe'},
                    component: () => import('@/components/views/taskCenter/MyDraft')
                },
                {
                    path: 'copyList',
                    name: 'copyList',
                    meta: {title: '抄送我的', icon: 'ios-cafe'},
                    component: () => import('@/components/views/taskCenter/CopyToMe')
                },
                {
                    path: 'model',
                    name: 'model',
                    meta: {title: '模型管理', icon: 'ios-cafe'},
                    component: () => import('@/components/views/workflow/ModelManager')
                },
                {
                    path: 'flowSetting',
                    name: 'flowSetting',
                    meta: {title: '流程设置', icon: 'ios-cafe'},
                    component: () => import('@/components/views/workflow/WorkflowManager')
                },
                {
                    path: 'noticeManager',
                    name: 'noticeManager',
                    meta: {title: '打印', icon: 'ios-cafe'},
                    component: () => import('@/components/views/notice/NoticeManager')
                }
            ]
        }
    ]
})
