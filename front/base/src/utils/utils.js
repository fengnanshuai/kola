import Vue from 'vue'
// import ElementUI from 'element-ui'

const _self = new Vue();

/**
 * 警告提示（1.5秒后自动关闭）
 * @param message 提示内容
 */
export const warningMsg = (message) => {
    _self.$message({showClose: false, message: message, type: 'warning', duration: 1500});
};

/**
 * 成功提示（1.5秒后自动关闭）
 * @param message 提示内容
 */
export const successMsg = (message) => {
    _self.$message({showClose: false, message: message, type: 'success', duration: 1500});
};

/**
 * 错误提示（1.5秒后自动关闭）
 * @param message 提示内容
 */
export const errorMsg = (message) => {
    _self.$message({showClose: false, message: message, type: 'error', duration: 1500});
};

/**
 * @param {Array} arr
 * @returns {Array}
 */
export const getBreadCrumbList = (arr) => {
    let res = arr.map(item => {
        return {
            icon: (item.meta && item.meta.icon) || '',
            name: item,
        }
    });
    return [{name: '主页', icon: 'ios-home-outline'}, ...res]
};


/**
 * 时间格式化
 * @param date
 * @param fmt
 * @returns {*}
 */
export function formatDate(date, fmt) {
    let o = {
        'M+': date.getMonth() + 1, // 月份
        'd+': date.getDate(), // 日
        'h+': date.getHours(), // 小时
        'm+': date.getMinutes(), // 分
        's+': date.getSeconds(), // 秒
        'S': date.getMilliseconds() // 毫秒
    };
    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length))
    }
    for (let k in o) {
        if (new RegExp('(' + k + ')').test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length)))
        }
    }
    return fmt
}


/**
 * 判断是否为null
 * @param str
 * @returns {string}
 */
export function judgeNull(str) {
    if (str === undefined || str === 'null' || str === null)
        return '';
}


/**
 * 将object 属性中 null值 转换为 ''
 * @param obj
 * @returns {string}
 */
export function objectExcludeNull(obj) {
    if (obj) {
        let resObj = {};
        for (let k of Object.keys(obj)) {
            resObj[k] = obj[k] === undefined || obj[k] === null || obj[k] === 'null' ? '' : obj[k];
        }
        return resObj;
    } else {
        return '';
    }
}

/**
 * 获取当前登录用户信息
 * @param key
 * @returns {any}
 */
export function getCurrUser(key) {
    return JSON.parse(sessionStorage.getItem(key));
}

/**
 * 获取当前用户id
 * @param key
 * @returns {*}
 */
export function getCurrUserId(key) {
    const parse = getCurrUser(key);
    return parse ? parse['id'] : '';
}

/**
 * 下载bpmn去除formData，修改camunda为activiti
 * @param json
 */
export function getFormProperty(json) {
    for (let e in json) {
        if (e === 'extensionElements' && json.extensionElements.formData && json.extensionElements.formData.formField) {
            json.extensionElements._businessKey = json.extensionElements.formData._businessKey
            let formProperty = JSON.parse(JSON.stringify(json.extensionElements.formData.formField))
            if (isArrayFn(formProperty)) {
                formProperty.forEach(x => {
                    x.__prefix = 'activiti'
                })
            } else {
                formProperty.__prefix = 'activiti'
            }
            json.extensionElements.formProperty = formProperty
            delete json.extensionElements.formData
        } else if (e.includes('camunda')) {
            let str = e.replace('camunda', 'activiti')
            json[str] = json[e]
            delete json[e]
        } else if (typeof json[e] == 'object') {
            getFormProperty(json[e])
        }
    }
}

/**
 * 判断是否是数组
 * @param value
 * @returns {arg is any[]|boolean}
 */
export function isArrayFn(value) {
    if (typeof Array.isArray === "function") {
        return Array.isArray(value);
    } else {
        return Object.prototype.toString.call(value) === "[object Array]";
    }
}

/**
 * 导入bpmn加上formData，修改activiti为camunda
 * @param json
 */
export function returnFormProperty(json) {
    for (let e in json) {
        if (e === 'extensionElements' && json.extensionElements.formProperty) {
            let formField = JSON.parse(JSON.stringify(json.extensionElements.formProperty))
            if (isArrayFn(formField)) {
                formField.forEach(x => {
                    x.__prefix = 'camunda'
                })
            } else {
                formField.__prefix = 'camunda'
            }
            json.extensionElements.formData = {
                formField,
                _businessKey: json.extensionElements._businessKey,
                __prefix: "camunda"
            }
            delete json.extensionElements.formProperty
        } else if (typeof json[e] == 'object') {
            returnFormProperty(json[e])
        }
    }
}