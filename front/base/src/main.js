/* eslint-disable */
import Vue from 'vue'
import App from './App.vue'
import router from './router/router'
import Vuex from "vuex";
import store from './store/index'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
//引入第三方自定义icon的css文件
import './assets/iconfont/iconfont.css'
import Print from 'vue-print-nb'
import _ from 'lodash'

import {
    axiosBaseInstance,
    axiosBaseInstanceJson,
    axiosIMInstance,
    axiosCommonInstance,
    axiosFlowInstance,
    COMMON_URL,
    BASE_URL,
    IM_URL,
    FLOW_URL,
    WSS_URL,
    $httpGet,
    $httpPost,
    $httpPut,
    $httpDelete
} from './utils/http'

import {warningMsg, successMsg, errorMsg, formatDate,getCurrUser,getCurrUserId} from './utils/utils'
import {createSocket, sendWSPush} from './utils/webSocketUtil'
import x2js from 'x2js' //xml数据处理插件
//注册
Vue.config.productionTip = false;
Vue.use(ElementUI);
Vue.use(Vuex);
Vue.use(Print);
//定义全局变量 (挂载vue原型链)
Vue.prototype.$x2js = new x2js() //创建x2js对象，挂到vue原型上
Vue.prototype.$axiosBaseHttp = axiosBaseInstance;
Vue.prototype.$axiosBaseHttpJson = axiosBaseInstanceJson;
Vue.prototype.$axiosIMInstance = axiosIMInstance;
Vue.prototype.$axiosCommonHttp = axiosCommonInstance;
Vue.prototype.$axiosFlowHttp = axiosFlowInstance;
Vue.prototype.$commonUrlHttp = COMMON_URL;
Vue.prototype.$baseUrlHttp = BASE_URL;
Vue.prototype.$imUrlHttp = IM_URL;
Vue.prototype.$flowUrlHttp = FLOW_URL;
Vue.prototype.$wssUrl = WSS_URL;
Vue.prototype.$httpGet = $httpGet;
Vue.prototype.$httpPost = $httpPost;
Vue.prototype.$httpPut = $httpPut;
Vue.prototype.$httpDelete = $httpDelete;
Vue.prototype.$formatDate = formatDate;
Vue.prototype.$warningMsg = warningMsg;
Vue.prototype.$successMsg = successMsg;
Vue.prototype.$errorMsg = errorMsg;
Vue.prototype.$wsConnection = createSocket;
Vue.prototype.$sendWSPush = sendWSPush;
Vue.prototype.$getCurrUser = getCurrUser;
Vue.prototype.$getCurrUserId = getCurrUserId;
Vue.prototype._ = _;

new Vue({
    render: h => h(App),
    router,
    store,
}).$mount('#app');
