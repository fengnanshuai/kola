package com.flow.server.modules.model.mapper;

import com.flow.server.modules.base.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 描述： 流程模型mapper
 * <p>
 * 作者：hutongfu
 * 时间：2020/1/18 11:23 星期六
 */
@Mapper
public interface ModelRestMapper extends BaseMapper {

    /**
     * 根据模型id删除信息
     *
     * @param modelId
     * @return
     */
    int deleteById(String modelId);

    /**
     * 根据id删除byteArray
     *
     * @param id
     * @return
     */
    int deleteByteArrayById(String id);

    /**
     * 根据部署id删除byteArray
     *
     * @param deployId
     * @return
     */
    int deleteByteArrayByDeployId(String deployId);

    /**
     * 根据部署id删除部署信息
     *
     * @param deployId
     * @return
     */
    int deleteDeployedById(String deployId);
}
