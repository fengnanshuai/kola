package com.flow.server.modules.task.service;

import com.flow.server.modules.base.service.IBaseService;

import java.util.Map;

/**
 * 描述：流程实例流转处理 service
 * <p>
 * 作者：Ostrich Hu
 * 时间：2020/1/20 9:39 星期一
 */
public interface ITaskRestService extends IBaseService {

    /**
     * 根据用户id和任务id锁定任务
     *
     * @param uId    用户id
     * @param taskId 任务id
     * @param type   {claim,unClaim}
     * @return
     */
    boolean ucTask(String uId, String taskId, String type);

    /**
     * 挂起/激活任务
     *
     * @param type       类型（suspended、active）
     * @param instanceId 流程实例id
     * @return
     */
    boolean asTask(String instanceId, String type);

    /**
     * 转办任务
     *
     * @param uId    用户id
     * @param taskId 任务id
     * @return
     */
    boolean turnTask(String uId, String taskId);

    /**
     * 办理任务
     *
     * @param uId    用户id
     * @param taskId 任务id
     * @param var    变量
     * @return
     */
    boolean doTask(String uId, String taskId, Map<String, Object> var);
}
