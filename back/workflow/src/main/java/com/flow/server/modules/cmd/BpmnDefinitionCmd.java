package com.flow.server.modules.cmd;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.Tolerate;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 描述：Bpmn 流程定义设置对象
 * <p>
 * 作者：Ostrich Hu
 * 时间：2019/9/3 15:08 星期二
 */
@Getter
@Setter
@Builder
@Accessors(chain = true)
public class BpmnDefinitionCmd implements Serializable {
    private static final long serialVersionUID = 8736113508927117956L;

    //流程模型id
    private String modelId;

    //流程定义key
    private String key;

    //节点id
    private String nodeId;

    //节点类型
    private String nodeType;

    //节点到期时间
    private String nodeDueDate;

    //节点到期时间(类型0:无 1:工作日 2:日历日)
    private String nodeDueDateType;

    //节点人员
    private List<Map> nodeUser;

    //节点抄送人员
    private List<Map> nodeCopyUser;

    //节点跳转类型
    private String nodeJumpType;

    //节点表单
    private List<Map> nodeForm;

    //节点按钮
    private String nodeBtn;

    //节点网关
    private List<Map> nodeGate;

    @Tolerate
    public BpmnDefinitionCmd() {
    }
}
