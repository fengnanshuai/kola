package com.flow.server.modules.cmd;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 描述：Bpmn处理命令实体
 * <p>
 * 作者：Ostrich Hu
 * 时间：2019/8/30 17:08 星期五
 */
@Getter
@Setter
@Builder
@Accessors(chain = true)
public class BpmnModelCmd implements Serializable {

    private static final long serialVersionUID = -6302229223356464471L;

    //流程定义key
    @NotNull
    private String key;

    //流程模型名称
    @NotNull
    private String name;

    //流程模型版本
    private int version;

    //流程模型分类
    private String category;

    //流程模型描述
    private String description;

    //流程模型部署状态(1:部署，0：未部署)
    private String status;

    //流程定义json
    @NotNull
    private String jsonXml;

    //流程定义svg
    @NotNull
    private String svgXml;

    //页码
    private int pageNum;

    //每页大小
    private int pageSize;
}
