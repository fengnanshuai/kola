package com.flow.server.common.utils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.List;

/**
 * @author HuTongFu
 * @description: 分页参数
 * @since 2019/6/6 13:39
 */
@Data
public class PageUtil<T> {

    private static final long serialVersionUID = 7473512058265628720L;

    private long pageNum;
    private long pageSize;
    private long startRow;
    private long endRow;
    private long total;
    private long pages;

    private List<T> result;

    public PageUtil(int pageNum, int pageSize) {
        this.pageNum = pageNum;
        this.pageSize = pageSize;
        this.startRow = pageNum > 0 ? (pageNum - 1) * pageSize : 0;
        this.endRow = pageNum * pageSize;
    }

    /**
     * 分页
     *
     * @param result   列表数据
     * @param total    总记录数
     * @param pageSize 每页记录数
     * @param pageNum  当前页数
     */
    public PageUtil(List<T> result, long total, long pageSize, long pageNum) {
        this.result = result;
        this.total = total;
        this.pageSize = pageSize;
        this.pageNum = pageNum;
        this.startRow = pageNum > 0 ? (pageNum - 1) * pageSize : 0;
        this.endRow = pageNum * pageSize;
        this.pages = pageSize != 0 ? (int) (total / pageSize + ((total % pageSize == 0) ? 0 : 1)) : 0;
    }

    /**
     * 上一页索引值
     *
     * @return
     */
    @JsonIgnore
    public long getPrev() {
        if (pageNum == 1) {
            return pageNum;
        } else {
            return pageNum - 1;
        }
    }

    /**
     * 下一页索引值
     *
     * @return
     */
    public long getNext() {
        if (pageNum == pages) {
            return pageNum;
        } else {
            return pageNum + 1;
        }
    }
}
