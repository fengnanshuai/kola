package com.flow.server.modules.instance.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 描述: ActTaskHistoryEntity 流程任务历史
 * <p>
 * 作者: hu_to
 * 时间: 2020/06/22 10:27
 */
@Data
public class ActTaskHistoryEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    //主键
    private String id;
    //任务名称
    private String taskName;
    //流程实例id
    private String instanceId;
    //任务办理人
    private String assignee;
    //任务删除理由
    private String deleteReason;
    //任务耗时
    private long duration;
    //任务开始时间
    private Date startTime;
    //任务结束时间
    private Date endTime;
    //流程分类
    private String category;
    //流程描述
    private String description;
    //审批意见
    private String comment;
}
