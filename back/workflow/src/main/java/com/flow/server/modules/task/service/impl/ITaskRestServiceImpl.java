package com.flow.server.modules.task.service.impl;

import com.flow.server.common.constant.Constant;
import com.flow.server.common.exception.SysWorkflowException;
import com.flow.server.modules.base.service.impl.IBaseServiceImpl;
import com.flow.server.modules.task.service.ITaskRestService;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 描述：流程实例流转处理 service 实现类
 * <p>
 * 作者：Ostrich Hu
 * 时间：2020/1/20 9:40 星期一
 */
@Service
public class ITaskRestServiceImpl extends IBaseServiceImpl implements ITaskRestService {

    @Override
    public boolean ucTask(String uId, String taskId, String type) {
        try {
            if (Constant.Flow.CLAIM.equalsIgnoreCase(type))
                taskService.claim(taskId, uId);
            else if (Constant.Flow.UN_CLAIM.equalsIgnoreCase(type))
                taskService.unclaim(taskId);
            return true;
        } catch (Exception e) {
            throw new SysWorkflowException(type + "任务失败！", e);
        }
    }

    @Override
    public boolean asTask(String instanceId, String type) {
        try {
            if (Constant.Flow.ACTIVE.equalsIgnoreCase(type))
                runtimeService.activateProcessInstanceById(instanceId);
            else if (Constant.Flow.SUSPENDED.equalsIgnoreCase(type))
                runtimeService.suspendProcessInstanceById(instanceId);
            return true;
        } catch (Exception e) {
            throw new SysWorkflowException(type + "失败！", e);
        }
    }

    @Override
    public boolean turnTask(String uId, String taskId) {
        return false;
    }

    @Override
    public boolean doTask(String uId, String taskId, Map<String, Object> var) {
        try {
            taskService.setAssignee(taskId, uId);
//            taskService.addComment(taskId,"","");
            taskService.complete(taskId, var);
            return true;
        } catch (Exception e) {
            throw new SysWorkflowException("办理任务失败！", e);
        }
    }
}
