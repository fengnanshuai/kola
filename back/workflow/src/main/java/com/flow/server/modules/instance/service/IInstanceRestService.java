package com.flow.server.modules.instance.service;

import com.flow.server.common.utils.PageUtil;
import com.flow.server.modules.base.service.IBaseService;
import com.flow.server.modules.cmd.ProcessInstanceCmd;
import com.flow.server.modules.instance.entity.ActProcessInstanceEntity;
import com.flow.server.modules.instance.entity.ActTaskHistoryEntity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 描述：流程实例service
 * <p>
 * 作者：Ostrich Hu
 * 时间：2020/1/19 9:13 星期日
 */
public interface IInstanceRestService extends IBaseService {

    /**
     * 发起流程实例
     *
     * @param request
     * @return
     */
    boolean startProcessInstance(HttpServletRequest request);

    /**
     * 分页查询流程实例
     *
     * @param request
     * @return
     */
    Map<String, Object> getListPage(HttpServletRequest request);

    /**
     * 根据流程实例id获取运行时流程图
     *
     * @param instanceId
     * @param response
     */
    void getRunWorkflowImage(String instanceId, HttpServletResponse response);

    /**
     * 根据条件查询办件中心流程实例
     *
     * @param processInstanceCmd 参数
     * @return list
     */
    PageUtil<ActProcessInstanceEntity> getInstanceListPage(ProcessInstanceCmd processInstanceCmd);

    /**
     * 查询流程任务历史信息
     * v 1.0.0
     * hu_to 2020-6-23 上午 10:28
     *
     * @param instanceId 流程实例id
     * @return list
     */
    List<ActTaskHistoryEntity> getTaskHistoryList(String instanceId);

    /**
     * 查询我的已办理的流程实例信息
     * v 1.0.0
     * hu_to 2020-6-24 下午 2:51
     *
     * @param processInstanceCmd 参数
     * @return page
     */
    PageUtil<ActProcessInstanceEntity> getMyDoneInstancePage(ProcessInstanceCmd processInstanceCmd);
}
