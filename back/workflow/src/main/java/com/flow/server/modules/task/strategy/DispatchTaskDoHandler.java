package com.flow.server.modules.task.strategy;

import com.flow.server.common.annotation.HandleType;
import org.springframework.stereotype.Component;

/**
 * 描述：任务分发处理器
 * <p>
 * 作者：Ostrich Hu
 * 时间：2020/1/20 10:30 星期一
 */
@HandleType("single")
@Component
public class DispatchTaskDoHandler extends AbstractTaskDoHandler {
}
