package com.flow.server.modules.definition.mapper;

import com.flow.server.modules.base.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 描述：流程定义mapper
 * <p>
 * 作者：hutongfu
 * 时间：2020/1/18 11:58 星期六
 */
@Mapper
public interface DefinitionMapper extends BaseMapper {

    /**
     * 根据部署id删除流程定义信息
     *
     * @param deploymentId 部署id
     * @return int
     */
    int deleteByDeployId(String deploymentId);
}
