package com.flow.server.modules.task.strategy;

import com.flow.server.common.mode.IStrategyHandleService;

/**
 * 描述：任务办理策略抽象类
 * <p>
 * 作者：Ostrich Hu
 * 时间：2020/1/20 10:26 星期一
 */
public abstract class AbstractTaskDoHandler implements IStrategyHandleService {
}
