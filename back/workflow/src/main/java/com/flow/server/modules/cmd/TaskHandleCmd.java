package com.flow.server.modules.cmd;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.Tolerate;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 描述：流程处理实体
 * <p>
 * 作者：hu_to
 * 时间：2019/8/10 18:30 星期六
 */
@Getter
@Setter
@Builder
@Accessors(chain = true)
public class TaskHandleCmd implements Serializable {
    private static final long serialVersionUID = 6638977712629823624L;

    //流程key
    private String key;

    //流程实例id
    private String instanceId;

    //流程定义id
    private String processDefId;

    //业务主键id
    private String businessId;

    //当前任务id
    private String currNodeId;

    //当前任务
    private String currNodeName;

    //当前任务key
    private String currNodeKey;

    //审批意见
    private String comment;

    //审批附件
    private String attachment;

    //操作类型（1：同意，2：驳回（上一级），3：驳回发起人，4：挂起，5：激活，6：作废）
    private String operateType;

    //审批人姓名
    private String operateUserName;

    //审批人id
    private String operateUserId;

    //下一节点候选人
    private List<Map> nextCandidateInfos;

    //下一节点抄送人
    private List<Map> nextCopyInfos;

    //操作时间
    @DateTimeFormat(pattern = "yyyy-MM-dd")
//    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date operateDate;

    //其他变量参数
    private Map<String, Object> variables;

    @Tolerate
    public TaskHandleCmd() {
    }
}
