package com.flow.server.modules.cmd;

import lombok.Data;

/**
 * 描述：基础Cmd
 * <p>
 * 作者：Ostrich Hu
 * 时间：2020/1/19 9:41 星期日
 */
@Data
public class BaseCmd {
}
