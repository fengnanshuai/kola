package com.flow.server.common.exception;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.flow.server.common.response.CommonResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.Set;

/**
 * 描述：系统异常处理器
 * <p>
 * 作者：HuTongFu
 * 时间：2019/7/12 15:51
 */
@ControllerAdvice
public class SysExceptionHandler {

    private Logger logger = (Logger) LoggerFactory.getLogger("error");

    {
        logger.setLevel(Level.ERROR);
    }

    /**
     * 捕捉其他所有异常
     */
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public CommonResponse handleException(Exception e, HttpServletRequest requests) {
        logger.error("错误日志记录");
        Map<String, String[]> dataMap = requests.getParameterMap();
        Set<String> keySet = dataMap.keySet();
        for (String key : keySet) {
            String value = StringUtils.join(dataMap.get(key), ",");
            logger.error("参数==>{}={}", key, value);//将请求参数保存在日志中
        }
        logger.error("错误日志记录===>\n{}", ExceptionUtils.getStackTrace(e));
        String msg = e.getMessage();
        if (msg == null || msg.equals("")) {
            msg = "系统内部错误";
        }
        return CommonResponse.error(msg);
    }

    /**
     * 捕捉文件所有异常
     */
    @ExceptionHandler(FastDFSException.class)
    @ResponseBody
    public CommonResponse handleFileException(Exception e, HttpServletRequest requests) {
        logger.error("错误日志记录");
        Map<String, String[]> dataMap = requests.getParameterMap();
        Set<String> keySet = dataMap.keySet();
        for (String key : keySet) {
            String value = StringUtils.join(dataMap.get(key), ",");
            logger.error("参数==>{}={}", key, value);//将请求参数保存在日志中
        }
        logger.error("错误日志记录===>\n{}", ExceptionUtils.getStackTrace(e));
        String msg = e.getMessage();
        if (msg == null || msg.equals("")) {
            msg = "文件系统内部错误";
        }
        return CommonResponse.error(msg);
    }

    /**
     * 捕捉手动抛出的所有异常
     */
    @ExceptionHandler(SysCommonException.class)
    @ResponseBody
    public CommonResponse handleCommonException(Exception e, HttpServletRequest requests) {
        Map<String, String[]> dataMap = requests.getParameterMap();
        Set<String> keySet = dataMap.keySet();
        for (String key : keySet) {
            String value = StringUtils.join(dataMap.get(key), ",");
            logger.error("参数==>{}={}", key, value);//将请求参数保存在日志中
        }
        logger.error("错误日志记录===>\n{}", ExceptionUtils.getStackTrace(e));
        String msg = e.getMessage();
        if (msg == null || msg.equals("")) {
            msg = "系统内部错误";
        }
        return CommonResponse.error(msg);
    }

    /**
     * 捕捉手动抛出的所有异常
     */
    @ExceptionHandler(SysWorkflowException.class)
    @ResponseBody
    public CommonResponse handleWorkflowException(Exception e, HttpServletRequest requests) {
        Map<String, String[]> dataMap = requests.getParameterMap();
        Set<String> keySet = dataMap.keySet();
        for (String key : keySet) {
            String value = StringUtils.join(dataMap.get(key), ",");
            logger.error("参数==>{}={}", key, value);//将请求参数保存在日志中
        }
        logger.error("错误日志记录===>\n{}", ExceptionUtils.getStackTrace(e));
        String msg = e.getMessage();
        if (msg == null || msg.equals("")) {
            msg = "流程引擎系统内部错误";
        }
        return CommonResponse.error(msg);
    }
}
