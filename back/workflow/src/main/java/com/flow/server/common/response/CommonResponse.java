package com.flow.server.common.response;

import com.flow.server.common.constant.Constant;

import java.util.HashMap;
import java.util.Map;

/**
 * 描述：通用返回对象
 * <p>
 * 作者：HuTongFu
 * 时间：2019/7/12 15:36
 */
public class CommonResponse extends HashMap<String, Object> {

    private static final long serialVersionUID = 6199093063476482538L;

    public CommonResponse() {
        put("code", Constant.Result.SUCCESS_CODE);
    }

    public CommonResponse(int code, String msg) {
        put("code", code);
        put("msg", msg);
    }

    public CommonResponse(int code, String msg, Object data) {
        put("code", code);
        put("msg", msg);
        put("data", data);
    }

    public static CommonResponse error() {
        return new CommonResponse(Constant.Result.FAIL_CODE, Constant.Result.FAIL_MSG);
    }

    public static CommonResponse error(String msg) {
        return error(Constant.Result.FAIL_CODE, msg);
    }

    public static CommonResponse error(int code, String msg) {
        CommonResponse r = new CommonResponse();
        r.put("code", code);
        r.put("msg", msg);
        return r;
    }

    public static CommonResponse ok(String msg) {
        CommonResponse r = new CommonResponse();
        r.put("msg", msg);
        return r;
    }

    public static CommonResponse ok(String code, String msg) {
        CommonResponse r = new CommonResponse();
        r.put("code", code);
        r.put("msg", msg);
        return r;
    }

    public static CommonResponse ok(Map<String, Object> map) {
        CommonResponse r = new CommonResponse();
        r.putAll(map);
        return r;
    }

    public static CommonResponse ok() {
        return new CommonResponse(Constant.Result.SUCCESS_CODE, Constant.Result.SUCCESS_MSG);
    }

    public static CommonResponse ok(Object data) {
        return new CommonResponse(Constant.Result.SUCCESS_CODE, Constant.Result.SUCCESS_MSG, data);
    }

    public CommonResponse put(String key, Object value) {
        super.put(key, value);
        return this;
    }

}
