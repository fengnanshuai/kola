package com.gateway.server.common.strategymode;

import com.gateway.server.common.annotation.StrategyType;
import com.gateway.server.common.constant.Constant;
import com.gateway.server.common.util.ClassScanner;
import com.google.common.collect.Maps;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 描述：HandlerProcessor将自定义的上下文容器注入到spring容器中
 * <p>
 * 作者：Ostrich Hu
 * 时间：2019/9/29 10:57 星期日
 */
@Component
public class HandlerProcessor implements BeanFactoryPostProcessor {

    /**
     * 将 自定义的 HandleContext 容器注入到spring中
     *
     * @param beanFactory
     * @throws BeansException
     */
    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        Map<String, Class> handleMap = Maps.newHashMapWithExpectedSize(3);
        //1.扫描指定包下标注了指定注解的类
        ClassScanner.scan(Constant.ScannerPackage.HANDLER_PACKAGE, StrategyType.class).forEach(clazz -> {
            //1.1获取注解中的类型值
            String annotationValue = clazz.getAnnotation(StrategyType.class).value();
            //1.2将注解中的类型值作为key，所标注的类做为value保存在自定义handleMap中
            handleMap.put(annotationValue, clazz);
        });
        //2.初始化 自定义spring上下文容器 HandlerContext并且注入到spring中
        HandlerContext handlerContext = new HandlerContext(handleMap);
        beanFactory.registerSingleton("handlerContext", handlerContext);
    }
}
