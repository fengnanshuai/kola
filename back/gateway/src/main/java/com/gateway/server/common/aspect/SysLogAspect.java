package com.gateway.server.common.aspect;

import com.gateway.server.common.config.SysCommonConfig;
import com.gateway.server.common.util.IPUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

@Aspect
@Component
public class SysLogAspect {

    private static final Logger logger = LoggerFactory.getLogger(SysLogAspect.class);

    @Autowired
    SysCommonConfig sysCommonConfig;

//    @Autowired
//    SysLogErrService sysLogErrService;

    @Pointcut("execution( * com.gateway.server..controller.*.*(..))")//两个..代表所有子目录，最后括号里的两个..代表所有参数
    public void logPointCut() {
    }

    @Before("logPointCut()")
    public void doBefore(JoinPoint joinPoint) throws Throwable {
        // 接收到请求，记录请求内容
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();

        // 记录下请求内容
        logger.info("请求地址 : " + request.getRequestURL().toString());
        logger.info("HTTP METHOD : " + request.getMethod());
        // 获取真实的ip地址
        logger.info("IP : " + IPUtils.getIpAddr(request));
        logger.info("CLASS_METHOD : " + joinPoint.getSignature().getDeclaringTypeName() + "."
                + joinPoint.getSignature().getName());
        logger.info("参数 : " + Arrays.toString(joinPoint.getArgs()));

    }

    @AfterReturning(returning = "ret", pointcut = "logPointCut()")// returning的值和doAfterReturning的参数名一致
    public void doAfterReturning(Object ret) throws Throwable {
        // 处理完请求，返回内容(返回值太复杂时，打印的是物理存储空间的地址)
        logger.info("返回值 : " + ret);
    }

    @Around("logPointCut()")
    public Object doAround(ProceedingJoinPoint pjp) throws Throwable {
        long startTime = System.currentTimeMillis();
        Object ob = pjp.proceed();// ob 为方法的返回值
        logger.info("耗时 : " + (System.currentTimeMillis() - startTime));
        return ob;
    }

    @AfterThrowing(pointcut = "logPointCut()", throwing = "ex")
    public void doAfterException(JoinPoint joinPoint, Exception ex) {
        saveLog(joinPoint, ex);
    }

    /**
     * 保存日志方法
     *
     * @param joinPoint 织点参数
     */
    private void saveLog(JoinPoint joinPoint, Throwable ex) {
//        SysLogErr sysLog = new SysLogErr();
//        try {
//            UserDto currUser = UserUtil.getUserFromRedis();
//            if (null == currUser) {
//                sysLog.setAccount("获取用户信息为空");
//            } else {
//                sysLog.setAccount(currUser.getId());
//            }
//        } catch (Exception e) {
//            sysLog.setAccount("获取用户信息为空");
//        }
//        // 获取request
//        HttpServletRequest request = HttpContextUtil.getHttpServletRequest();
//        sysLog.setSysType(sysCommonConfig.getSysType());
//        sysLog.setRequestParam(Arrays.toString(joinPoint.getArgs()));
//        sysLog.setUrl(request.getRequestURI());
//        sysLog.setIpAddress(request.getLocalAddr());
//        sysLog.setIpSource(IPUtils.getIpAddr(request));
//        sysLog.setContent(ex.getMessage());
//        sysLog.setStackTrace(JSON.toJSONString(ex.getStackTrace()));
//        sysLog.setCreateTime(LocalDateTime.now());
//        sysLog.setId(StringUtils.uuidGenerator());
//        sysLogErrService.save(sysLog);
    }
}
