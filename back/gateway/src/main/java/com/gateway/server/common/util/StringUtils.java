package com.gateway.server.common.util;

import com.fasterxml.uuid.EthernetAddress;
import com.fasterxml.uuid.Generators;

import java.util.UUID;


/**
 * 描述：字符串工具类
 * <p>
 * 作者：HuTongFu
 * 时间：2019/7/12 15:34
 */
public class StringUtils extends org.apache.commons.lang3.StringUtils {
    private static final String AND = "&&";
    private static final String OR = "||";

    public static String uuidGenerator() {
        UUID uuid = Generators.timeBasedGenerator(EthernetAddress.fromInterface()).generate();
        return String.valueOf(uuid.timestamp());
    }

    /**
     * 将 null 转为 ""
     *
     * @param param 带转换参数
     * @return 转换之后的结果
     */
    public static String convertNull2Str(String param) {
        return org.apache.commons.lang3.StringUtils.isBlank(param) ? "" : param;
    }

    /**
     * 将 null 转为 ""
     *
     * @param param 带转换参数
     * @return 转换之后的结果
     */
    public static String convertNull2Str(Object param) {
        return param == null ? null : param.toString();
    }

    /**
     * 判断字符传表达式是否成立
     *
     * @param condition
     * @return
     */
    public static boolean isTrue(String condition) {
        if (condition.toLowerCase().equals("true")) {
            return true;
        } else if (condition.toLowerCase().equals("false")) {
            return false;
        } else {
            condition = condition.substring(1, condition.length() - 1);
            String[] ands = condition.split("and");
            int count = 0;
            for (String and : ands) {
                String[] ors = and.split("or");
                for (String or : ors) {
                    if (or.toLowerCase().trim().equals("true")) {
                        count++;
                        break;
                    }
                }
            }
            return count == ands.length;
        }
    }
}
