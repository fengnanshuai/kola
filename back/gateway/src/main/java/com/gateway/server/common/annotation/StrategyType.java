package com.gateway.server.common.annotation;

import java.lang.annotation.*;

/**
 * 描述：自定义策略模式注解：用于指定条件
 * <p>
 * 作者：Ostrich Hu
 * 时间：2019/9/29 10:10 星期日
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.TYPE})
@Inherited
public @interface StrategyType {

    String value();
}
