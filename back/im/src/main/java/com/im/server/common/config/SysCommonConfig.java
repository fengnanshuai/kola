package com.im.server.common.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * 描述：系统公共配置类
 * <p>
 * 作者：HuTongFu
 * 时间：2019/6/25 10:14
 */
@Configuration
@ConfigurationProperties(prefix = "sys.config")
@Data
public class SysCommonConfig {

    //注册中心地址
    private String registryServerAddress;

    //服务调用url
    private Map<String, String> feignUrl = new HashMap<>();

    //服务地址，用于配置服务发布所在机器的域名
    private String serverUri;

    //系统类型
    private String sysType;

    //文件上传的路径
    private String uploadFilePath;

    //swagger2 开关
    private boolean swagger2Enable;

    //文件权限开关
    private boolean accessTokenEnable;

    //netty 端口
    private int nettyPort;
}
