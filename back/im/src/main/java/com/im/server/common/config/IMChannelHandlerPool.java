package com.im.server.common.config;

import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;

/**
 * 描述：即时通讯通道组池，管理所有 websocket连接
 * <p>
 * 作者：Ostrich Hu
 * 时间：2019/12/24 14:04 星期二
 */
public class IMChannelHandlerPool {

    public IMChannelHandlerPool() {
    }

    public static ChannelGroup channelGroup = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
}
