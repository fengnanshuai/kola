package com.im.server.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

/**
 * 描述：WebSocket 配置类
 * <p>
 * 作者：Ostrich Hu
 * 时间：2019/9/17 17:31 星期二
 */
@Deprecated
@Configuration
public class WebSocketConfig {

    /**
     * 注册 服务端口类
     *
     * @return
     */
    @Bean
    public ServerEndpointExporter serverEndpointExporter() {
        return new ServerEndpointExporter();
    }
}
