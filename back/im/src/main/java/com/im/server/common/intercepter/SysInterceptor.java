//package com.im.server.common.intercepter;
//
//import com.alibaba.fastjson.JSON;
//import com.im.server.common.constant.Constant;
//import com.im.server.common.exception.SysCommonException;
//import com.im.server.common.response.CommonResponse;
//import com.im.server.common.utils.HttpContextUtil;
//import org.apache.commons.lang3.StringUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.http.HttpStatus;
//import org.springframework.stereotype.Component;
//import org.springframework.web.servlet.HandlerInterceptor;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.io.PrintWriter;
//
///**
// * 描述：自定义请求拦截器
// * <p>
// * 作者：HuTongFu
// * 时间：2019/7/12 10:37
// */
//@Component
//public class SysInterceptor implements HandlerInterceptor {
//
//    private final Logger logger = LoggerFactory.getLogger(this.getClass());
//
//    //如果false，停止流程，api被拦截
//    @Override
//    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object o) throws Exception {
//        //判断请求类型，是否是ajax请求或者来自浏览器的请求，如果是则直接放过，否则进行token验证
//        if (HttpContextUtil.isAjax(request) || !HttpContextUtil.isFromBrowse(request)) {
//            return true;
//        }
//        String authorization = request.getHeader("Authorization");
//        if (StringUtils.isEmpty(authorization)) {
////            this.response401(response, request.getRequestURI());
////            return false;
//            return true;
//        }
//        return true;
//    }
//
//    @Override
//    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {
//
//    }
//
//    @Override
//    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {
//
//    }
//
//    private void response401(HttpServletResponse response, String uri) {
//        response.setStatus(HttpStatus.UNAUTHORIZED.value());
//        response.setCharacterEncoding("UTF-8");
//        response.setContentType("application/json; charset=utf-8");
//        try (PrintWriter out = response.getWriter()) {
//            CommonResponse resVo = new CommonResponse(Constant.Permission.UN_LOGIN, "无权访问(Unauthorized):" + uri);
//            out.append(JSON.toJSONString(resVo));
//        } catch (IOException e) {
//            throw new SysCommonException("直接返回Response信息出现IOException异常:", e);
//        }
//    }
//}
