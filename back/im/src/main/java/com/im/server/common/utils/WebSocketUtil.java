package com.im.server.common.utils;

import com.im.server.common.config.IMChannelHandlerPool;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelId;
import io.netty.channel.group.ChannelGroupFuture;

import java.util.HashMap;
import java.util.Map;

/**
 * 描述：websocket 工具类
 * <p>
 * 作者：Ostrich Hu
 * 时间：2019/12/24 14:49 星期二
 */
public class WebSocketUtil {

    /**
     * 获取websocket url参数
     *
     * @param url url参数
     * @return Map
     */
    public static Map getUrlParams(String url) {
        Map<String, String> map = new HashMap<>();
        url = url.replace("?", ";");
        if (!url.contains(";")) {
            return map;
        }
        if (url.split(";").length > 0) {
            String[] arr = url.split(";")[1].split("&");
            for (String s : arr) {
                String key = s.split("=")[0];
                String value = s.split("=")[1];
                map.put(key, value);
            }
            return map;
        } else {
            return map;
        }
    }

    /**
     * 收到信息后，群发给所有 channel
     *
     * @param message 消息
     * @return ChannelGroupFuture
     */
    public static ChannelGroupFuture sendBroadcast(Object message) {
        return IMChannelHandlerPool.channelGroup.writeAndFlush(message);
    }

    /**
     * 收到消息后，发送给指定的 channel
     *
     * @param channelId 通道id
     * @param message   消息
     * @return ChannelFuture
     */
    public static ChannelFuture sendToTarget(ChannelId channelId, Object message) {
        Channel channel = IMChannelHandlerPool.channelGroup.find(channelId);
        return channel.writeAndFlush(message);
    }

    /**
     * channel ping
     *
     * @param channelId 通道id
     */
    public static void ping(ChannelId channelId) {
        sendToTarget(channelId, "pong");
    }
}
