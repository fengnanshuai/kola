package com.common.server.modules.log.mapper;

import com.common.server.modules.log.entity.SysLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统日志表 Mapper 接口
 * </p>
 *
 * @author ostrich
 * @since 2019-11-22
 */
public interface SysLogMapper extends BaseMapper<SysLog> {

}
