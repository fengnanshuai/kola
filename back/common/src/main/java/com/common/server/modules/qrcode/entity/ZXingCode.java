package com.common.server.modules.qrcode.entity;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.common.server.common.config.zxingcode.LogoConfig;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Map;

/**
 * 描述：二维码配置信息
 * <p>
 * 作者：Ostrich Hu
 * 时间：2019/10/12 17:53 星期六
 */
@Data
@ApiModel(value = "二维码配置对象", description = "二维码配置")
public class ZXingCode {

    @ApiModelProperty(value = "是否添加Log图片")
    private boolean logoFlg = false;

    @ApiModelProperty(value = "二维码编码内容", required = true)
    private String content;

    @ApiModelProperty(hidden = true)
    private BarcodeFormat barcodeformat = BarcodeFormat.QR_CODE;

    @ApiModelProperty(value = "生成图片宽度")
    private int width = 200;

    @ApiModelProperty(value = "生成图片高度")
    private int height = 200;

    @ApiModelProperty(hidden = true)
    private Map<EncodeHintType, ?> hints;

    @ApiModelProperty(value = "Logo图片路径")
    private String logoPath;

    @ApiModelProperty(value = "图片输出路径")
    private String putPath = "ZXingCodeTemp";

    @ApiModelProperty(hidden = true)
    private LogoConfig logoConfig;
}
