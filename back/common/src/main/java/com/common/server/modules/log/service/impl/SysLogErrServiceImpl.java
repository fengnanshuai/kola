package com.common.server.modules.log.service.impl;

import com.common.server.modules.log.entity.SysLogErr;
import com.common.server.modules.log.mapper.SysLogErrMapper;
import com.common.server.modules.log.service.SysLogErrService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统异常日志 服务实现类
 * </p>
 *
 * @author ostrich
 * @since 2019-10-09
 */
@Service
public class SysLogErrServiceImpl extends ServiceImpl<SysLogErrMapper, SysLogErr> implements SysLogErrService {

}
