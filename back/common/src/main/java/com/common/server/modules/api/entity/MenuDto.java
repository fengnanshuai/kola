package com.common.server.modules.api.entity;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @author HuTongFu
 * @description: 菜单权限
 * @since 2019/6/6 18:11
 */
@Data
public class MenuDto implements Serializable {

    private static final long serialVersionUID = -6050945812825348644L;

    //菜单（权限）id
    private String id;

    //菜单（权限）名称
    private String name;

    //菜单（权限）描述
    private String description;

    //父级菜单（权限）id
    private String pId;

    //菜单（权限）编码
    private String code;

    //类型：（1菜单、2权限）
    private Integer type;

    //菜单url
    private String url;

    //排序序号
    private Integer orderIndex;

    //状态
    private Integer status;

    //添加时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date addDate;

    //修改时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateDate;

    //是否删除（1是、0否）
    private Integer isDelete;

    //修改人
    private String updateUser;

    //添加人
    private String addUser;
}
