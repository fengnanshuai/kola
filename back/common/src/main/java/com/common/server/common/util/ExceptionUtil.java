package com.common.server.common.util;


/**
 * 描述：异常工具类
 * <p>
 * 作者：HuTongFu
 * 时间：2019/7/12 15:36
 */
public class ExceptionUtil {

    public static String getExceptionAllinformation(Exception ex) {
        String sOut = "";
        StackTraceElement[] trace = ex.getStackTrace();
        for (StackTraceElement s : trace) {
            sOut += "\tat " + s + "\r\n";
        }
        return sOut;
    }
}
