package com.common.server.modules.file.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.time.LocalDateTime;
import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 文件信息表
 * </p>
 *
 * @author ostrich
 * @since 2019-10-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_common_file")
@ApiModel(value = "File对象", description = "文件信息表")
public class FileEntity extends Model<FileEntity> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    private String id;

    @ApiModelProperty(value = "文件编码")
    private String code;

    @ApiModelProperty(value = "服务器ip")
    private String ip;

    @ApiModelProperty(value = "文件url")
    private String url;

    @ApiModelProperty(value = "文件原名")
    private String oldName;

    @ApiModelProperty(value = "文件名")
    private String name;

    @ApiModelProperty(value = "文件类型")
    private String type;

    @ApiModelProperty(value = "文件大小")
    private String size;

    @ApiModelProperty(value = "文件排序")
    private String orderIndex;

    @ApiModelProperty(value = "添加人")
    private String addUser;

    @ApiModelProperty(value = "添加时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime addDate;

    @ApiModelProperty(value = "状态")
    private String status;

    @ApiModelProperty(value = "备注")
    private String remark;

    @Override
    protected Serializable pkVal() {
        return null;
    }

}
