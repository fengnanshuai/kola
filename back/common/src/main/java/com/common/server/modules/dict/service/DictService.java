package com.common.server.modules.dict.service;

import com.common.server.common.util.Tree;
import com.common.server.modules.dict.entity.Dict;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 字典表 服务类
 * </p>
 *
 * @author ostrich
 * @since 2019-10-09
 */
public interface DictService extends IService<Dict> {

    List<Tree<Dict>> getDictTree(Dict dict);

    List<Map<String, Object>> getKVList(String type);
}
