package com.common.server.modules.log.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 系统异常日志
 * </p>
 *
 * @author ostrich
 * @since 2019-10-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_sys_log_err")
@ApiModel(value="SysLogErr对象", description="系统异常日志")
public class SysLogErr extends Model<SysLogErr> {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    private String id;

    @ApiModelProperty(value = "系统类型")
    private String sysType;

    @ApiModelProperty(value = "帐号")
    private String account;

    @ApiModelProperty(value = "IP来源")
    private String ipSource;

    @ApiModelProperty(value = "IP地址")
    private String ipAddress;

    @ApiModelProperty(value = "状态：unchecked，checked，fixed")
    private String status;

    @ApiModelProperty(value = "错误URL")
    private String url;

    @ApiModelProperty(value = "出错信息")
    private String content;

    @ApiModelProperty(value = "请求参数")
    private String requestParam;

    @ApiModelProperty(value = "出错时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "出错异常堆栈")
    private String stackTrace;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
