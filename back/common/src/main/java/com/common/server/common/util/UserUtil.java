package com.common.server.common.util;

import com.alibaba.fastjson.JSON;
import com.common.server.common.config.redis.RedisClient;
import com.common.server.common.constant.Constant;
import com.common.server.common.exception.CustomerException;
import com.common.server.common.response.ResponseVo;
import com.common.server.modules.api.PermissionApi;
import com.common.server.modules.api.entity.UserDto;

import javax.servlet.http.HttpServletRequest;

/**
 * 描述： 用户工具类(获取当前登录的用户信息)
 * <p>
 * 作者：Ostrich Hu
 * 时间：2019/8/1 14:17
 */
public class UserUtil {

    /**
     * 从 Redis 获取当前用户信息
     */
    public static UserDto getUserFromRedis() {
        HttpServletRequest httpRequest = HttpContextUtil.getHttpServletRequest();
        String token = httpRequest.getHeader("Authorization");
//        if (StringUtils.isEmpty(token)) {
            throw new CustomerException("请求头中 Authorization 为空");
//        }
        //校验token 从校验的结果中获取 登录账号
//        PermissionApi api = new FeignUtil<PermissionApi>().invokeService(PermissionApi.class);
//        ResponseVo response = api.validToken(token);
//        if (Constant.Permission.SUCCESS == Integer.parseInt(response.get("code").toString())) {
//            String account = JSON.parseObject(JSON.toJSONString(response.get("data")), String.class);
//            RedisClient redisClient = (RedisClient) ContextFactoryUtil.getBean("redisClient");
//            if (redisClient != null) {
//                String key = Constant.RedisConstant.PREFIX_TOKEN_U_CACHE + account;
//                if (redisClient.hasKey(key)) {
//                    return JSON.parseObject(String.valueOf(redisClient.get(key)), UserDto.class);
//                } else {
//                    throw new CustomerException("获取当前登录用户失败");
//                }
//            } else {
//                throw new CustomerException("获取 redisClient 组件失败");
//            }
//        } else {
//            throw new CustomerException(response.get("msg").toString());
//        }
    }


}
