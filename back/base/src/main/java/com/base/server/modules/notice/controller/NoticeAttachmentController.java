package com.base.server.modules.notice.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.base.server.common.exception.SysCommonException;
import com.base.server.common.response.CommonResponse;
import com.base.server.common.utils.UuidUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;

import com.base.server.modules.notice.service.NoticeAttachmentService;
import com.base.server.modules.notice.entity.NoticeAttachment;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;
/**
 * <p>
 * 通知附件表 前端控制器
 * </p>
 *
 * 作者: ostrich
 * 时间: 2020-01-16
 */
@Api(tags = "通知附件表")
@RestController
@RequestMapping("api/rest/noticeAttachment")
public class NoticeAttachmentController {
    private final Logger logger=LoggerFactory.getLogger(NoticeAttachmentController.class);

    @Autowired
    public NoticeAttachmentService iNoticeAttachmentService;

    @ApiOperation("无分页查询数据")
    @GetMapping(value = "/list")
    public CommonResponse getNoticeAttachmentList(NoticeAttachment noticeAttachment) {
        try {
            QueryWrapper<NoticeAttachment> queryWrapper = new QueryWrapper<>(noticeAttachment);
            List<NoticeAttachment> result = iNoticeAttachmentService.list(queryWrapper);
            return CommonResponse.ok(result);
        } catch (Exception e) {
            throw new SysCommonException("无分页查询数据异常", e);
        }
    }

    @ApiOperation("分页查询数据")
    @GetMapping("/list/page")
    public CommonResponse getNoticeAttachmentListPage(NoticeAttachment noticeAttachment){
        try {
            Page<NoticeAttachment> page = new Page<>();
            QueryWrapper<NoticeAttachment> queryWrapper = new QueryWrapper<>(noticeAttachment);
            IPage<NoticeAttachment> result = iNoticeAttachmentService.page(page, queryWrapper);
        return CommonResponse.ok(result);
        } catch (Exception e) {
            throw new SysCommonException("分页查询数据异常", e);
        }
    }

//    @ApiOperation("新增数据")
//    @PostMapping(value = "/save")
//    public CommonResponse noticeAttachmentSave(NoticeAttachment noticeAttachment){
//        try{
//            noticeAttachment.setId(UuidUtil.randomUUID());
//            iNoticeAttachmentService.save(noticeAttachment);
//            return CommonResponse.ok(noticeAttachment);
//        }catch(Exception e){
//            throw new SysCommonException("新增数据异常", e);
//        }
//    }

    @ApiOperation("修改数据")
    @PutMapping(value = "/upd")
    public CommonResponse noticeAttachmentUpdate(NoticeAttachment noticeAttachment){
        try{
            iNoticeAttachmentService.save(noticeAttachment);
            return CommonResponse.ok(noticeAttachment);
        }catch(Exception e){
            throw new SysCommonException("修改数据异常", e);
        }
    }

    @ApiOperation("根据id删除对象")
    @DeleteMapping(value = "del/{id}")
    public CommonResponse noticeAttachmentDelete(@PathVariable String id){
        try{
            int count = iNoticeAttachmentService.removeById(id) ? 1 : 0;
            return CommonResponse.ok(count);
        }catch(Exception e){
            throw new SysCommonException("根据id删除对象异常", e);
        }
    }

    @ApiOperation("批量删除对象")
    @DeleteMapping(value = "del/batch")
    public CommonResponse deleteBatchIds(@RequestParam List<Long> ids){
        try{
            int count = iNoticeAttachmentService.removeByIds(ids)?1:0;
            return CommonResponse.ok(count);
        }catch(Exception e){
            throw new SysCommonException("批量删除对象异常", e);
        }
     }
}

