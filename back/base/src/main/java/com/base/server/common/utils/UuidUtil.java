package com.base.server.common.utils;

import com.fasterxml.uuid.EthernetAddress;
import com.fasterxml.uuid.Generators;

import java.util.UUID;

/**
 * 描述：UUID 生成工具类
 * <p>
 * 作者：Ostrich Hu
 * 时间：2019/11/28 15:05 星期四
 */
public class UuidUtil {
    public static String randomUUID() {
        UUID uuid = Generators.timeBasedGenerator(EthernetAddress.fromInterface()).generate();
        return String.valueOf(uuid.timestamp());
    }

}
