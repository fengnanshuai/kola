package com.base.server.modules.user.service.impl;

import com.base.server.modules.user.entity.UserRoleRelation;
import com.base.server.modules.user.mapper.UserRoleRelationMapper;
import com.base.server.modules.user.service.UserRoleRelationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户与角色关联表 服务实现类
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
@Service
public class UserRoleRelationServiceImpl extends ServiceImpl<UserRoleRelationMapper, UserRoleRelation> implements UserRoleRelationService {

}
