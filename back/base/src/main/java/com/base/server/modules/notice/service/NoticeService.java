package com.base.server.modules.notice.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.base.server.modules.notice.entity.Notice;
import com.baomidou.mybatisplus.extension.service.IService;
import com.base.server.modules.notice.entity.NoticeVo;

/**
 * <p>
 * 通知表 服务类
 * </p>
 *
 * @author ostrich
 * @since 2020-01-16
 */
public interface NoticeService extends IService<Notice> {

    /**
     * 分页查询
     *
     * @param page
     * @param noticeVo
     * @return
     */
    IPage<NoticeVo> selectByMyPage(Page<NoticeVo> page, NoticeVo noticeVo);

    /**
     * 根据条件单条查询
     *
     * @param noticeVo
     * @return
     */
    NoticeVo selectNoticeOne(NoticeVo noticeVo);

    /**
     * 保存通知
     *
     * @param noticeVo
     * @return
     */
    NoticeVo save(NoticeVo noticeVo);

    /**
     * 修改通知
     *
     * @param noticeVo
     * @return
     */
    NoticeVo noticeUpdate(NoticeVo noticeVo);
}
