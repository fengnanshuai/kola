package com.base.server.common.config.jwt;

import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.cache.CacheManager;

/**
 * 描述：重写 shiro 缓存管理器
 * <p>
 * 作者：HuTongFu
 * 时间：2019/7/11 11:11
 */
public class CustomCacheManager implements CacheManager {
    @Override
    public <K, V> Cache<K, V> getCache(String name) throws CacheException {

        return new CustomCache<K, V>();
    }
}
