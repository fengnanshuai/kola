package com.base.server.modules.user.mapper;

import com.base.server.modules.user.entity.UserGroupRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户组与用户关联表 Mapper 接口
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
public interface UserGroupRelationMapper extends BaseMapper<UserGroupRelation> {

    /**
     * 根据用户id删除关系
     *
     * @param uid 用户id
     */
    void deleteByUId(String uid);
}
