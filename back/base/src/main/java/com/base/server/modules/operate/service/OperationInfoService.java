package com.base.server.modules.operate.service;

import com.base.server.modules.operate.entity.OperationInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 功能操作表 服务类
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
public interface OperationInfoService extends IService<OperationInfo> {

}
