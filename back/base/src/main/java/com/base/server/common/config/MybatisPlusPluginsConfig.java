package com.base.server.common.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 描述：mybatisPlus 插件配置类
 * <p>
 * 作者：Ostrich Hu
 * 时间：2019/9/30 11:23 星期一
 */
@Configuration
public class MybatisPlusPluginsConfig {

    /**
     * 分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        PaginationInterceptor paginationInterceptor = new PaginationInterceptor();
        paginationInterceptor.setLimit(-1);
        paginationInterceptor.setDialectType("mysql");
        return paginationInterceptor;
    }
}
