package com.base.server.modules.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.base.server.common.utils.MD5Utils;
import com.base.server.common.utils.UuidUtil;
import com.base.server.modules.user.entity.UserGroupRelation;
import com.base.server.modules.user.entity.UserInfo;
import com.base.server.modules.user.entity.UserInfoVo;
import com.base.server.modules.user.entity.UserRoleRelation;
import com.base.server.modules.user.mapper.UserGroupRelationMapper;
import com.base.server.modules.user.mapper.UserInfoMapper;
import com.base.server.modules.user.mapper.UserRoleRelationMapper;
import com.base.server.modules.user.service.UserInfoService;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
@Service
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo> implements UserInfoService {

    @Autowired
    private UserInfoMapper userInfoMapper;

    @Autowired
    private UserRoleRelationMapper userRoleRelationMapper;

    @Autowired
    private UserGroupRelationMapper userGroupRelationMapper;

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    @Override
    public boolean save(UserInfoVo userInfoVo) {
        UserInfo userInfo = new UserInfo();
        BeanUtils.copyProperties(userInfoVo, userInfo);
        userInfo.setId(UuidUtil.randomUUID());
        userInfo.setAddTime(LocalDateTime.now());
        userInfo.setPassword(MD5Utils.encrypt(userInfo.getPassword()));
        //新增用户
        boolean b = userInfoMapper.insert(userInfo) > 0;
        if (b) {
            //新增用户和角色关系
            userRoleRelationMapper.deleteByUId(userInfo.getId());
            List<String> roleIds = Arrays.asList(userInfoVo.getRoleId().split(","));
            roleIds.parallelStream().forEach(roleId -> {
                UserRoleRelation userRoleRelation = new UserRoleRelation();
                userRoleRelation.setId(UuidUtil.randomUUID());
                userRoleRelation.setUserId(userInfo.getId());
                userRoleRelation.setRoleId(roleId);
                userRoleRelationMapper.insert(userRoleRelation);
            });
            //新增用户和用户组关系
            userGroupRelationMapper.deleteByUId(userInfo.getId());
            List<String> groupIds = Arrays.asList(userInfoVo.getGroupId().split(","));
            groupIds.parallelStream().forEach(groupId -> {
                UserGroupRelation userGroupRelation = new UserGroupRelation();
                userGroupRelation.setId(UuidUtil.randomUUID());
                userGroupRelation.setUserId(userInfo.getId());
                userGroupRelation.setUserGroupId(groupId);
                userGroupRelationMapper.insert(userGroupRelation);
            });
        }
        return b;
    }

    @Override
    public boolean updateById(UserInfoVo userInfoVo) {
        UserInfo userInfo = new UserInfo();
        BeanUtils.copyProperties(userInfoVo, userInfo);
        if (StringUtils.isNoneBlank(userInfo.getPassword())) {
//            userInfo.setPassword(MD5Utils.encrypt(userInfo.getPassword()));
        }
        userInfo.setUpdateTime(LocalDateTime.now());
        //修改用户信息
        boolean b = userInfoMapper.updateById(userInfo) > 0;
        if (b) {
            //新增用户和角色关系
            userRoleRelationMapper.deleteByUId(userInfo.getId());
            List<String> roleIds = Arrays.asList(userInfoVo.getRoleId().split(","));
            roleIds.parallelStream().forEach(roleId -> {
                UserRoleRelation userRoleRelation = new UserRoleRelation();
                userRoleRelation.setId(UuidUtil.randomUUID());
                userRoleRelation.setUserId(userInfo.getId());
                userRoleRelation.setRoleId(roleId);
                userRoleRelationMapper.insert(userRoleRelation);
            });
            //新增用户和用户组关系
            userGroupRelationMapper.deleteByUId(userInfo.getId());
            List<String> groupIds = Arrays.asList(userInfoVo.getGroupId().split(","));
            groupIds.parallelStream().forEach(groupId -> {
                UserGroupRelation userGroupRelation = new UserGroupRelation();
                userGroupRelation.setId(UuidUtil.randomUUID());
                userGroupRelation.setUserId(userInfo.getId());
                userGroupRelation.setUserGroupId(groupId);
                userGroupRelationMapper.insert(userGroupRelation);
            });
        }
        return b;
    }

    @Override
    public List<UserInfoVo> list(UserInfoVo userInfoVo) {
        QueryWrapper<UserInfoVo> queryWrapper = new QueryWrapper<>(userInfoVo);
        if (StringUtils.isNoneBlank(userInfoVo.getGroupId())) {
            queryWrapper.eq("ugi.id", userInfoVo.getGroupId());
        }
        return userInfoMapper.selectList(queryWrapper);
    }

    @Override
    public UserInfoVo selectOne(UserInfoVo userInfoVo) {
        QueryWrapper<UserInfoVo> queryWrapper = new QueryWrapper<>(userInfoVo);
        if (StringUtils.isNoneBlank(userInfoVo.getId())) {
            queryWrapper.eq("ui.id", userInfoVo.getId());
        }
        return userInfoMapper.selectOne(queryWrapper);
    }

    @Override
    public boolean userInfoUpdateByOne(UserInfoVo userInfoVo) {
        UserInfo userInfo = new UserInfo();
        BeanUtils.copyProperties(userInfoVo, userInfo);
        if (StringUtils.isNoneBlank(userInfoVo.getNewPassword())) {
            userInfo.setPassword(MD5Utils.encrypt(userInfoVo.getNewPassword()));
        }
        userInfo.setUpdateTime(LocalDateTime.now());
        //修改用户信息
        return userInfoMapper.updateById(userInfo) > 0;
    }

    @Override
    public IPage<UserInfoVo> selectByMyPage(Page<UserInfoVo> page, UserInfoVo userInfoVo) {
        QueryWrapper<UserInfoVo> queryWrapper = new QueryWrapper<>(userInfoVo);
        queryWrapper.like("ui.name", userInfoVo.getName());
        queryWrapper.like("ui.sex", userInfoVo.getSex());
        return userInfoMapper.selectMyPage(page, queryWrapper);
    }
}
