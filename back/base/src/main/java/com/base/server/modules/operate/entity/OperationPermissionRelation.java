package com.base.server.modules.operate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 权限与功能操作关联表
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_operation_permission_relation")
@ApiModel(value="OperationPermissionRelation对象", description="权限与功能操作关联表")
public class OperationPermissionRelation extends Model<OperationPermissionRelation> {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键id")
    private String id;

    @ApiModelProperty(value = "功能操作id")
    private String operateId;

    @ApiModelProperty(value = "权限id")
    private String permissionId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
