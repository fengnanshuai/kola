package com.base.server.modules.operate.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.base.server.common.exception.SysCommonException;
import com.base.server.common.response.CommonResponse;
import com.base.server.common.utils.UuidUtil;
import com.base.server.modules.operate.entity.OperationInfo;
import com.base.server.modules.operate.service.OperationInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 功能操作表 前端控制器
 * </p>
 * <p>
 * 作者: ostrich
 * 时间: 2019-11-28
 */
@Api(tags = "功能操作表")
@RestController
@RequestMapping("api/rest/operationInfo")
public class OperationInfoController {
    private final Logger logger = LoggerFactory.getLogger(OperationInfoController.class);

    @Autowired
    public OperationInfoService iOperationInfoService;

    @ApiOperation("无分页查询数据")
    @GetMapping(value = "/list")
    public CommonResponse getOperationInfoList(OperationInfo operationInfo) {
        try {
            QueryWrapper<OperationInfo> queryWrapper = new QueryWrapper<>(operationInfo);
            List<OperationInfo> result = iOperationInfoService.list(queryWrapper);
            return CommonResponse.ok(result);
        } catch (Exception e) {
            throw new SysCommonException("无分页查询数据异常", e);
        }
    }

    @ApiOperation("分页查询数据")
    @GetMapping("/list/page")
    public CommonResponse getOperationInfoListPage(OperationInfo operationInfo) {
        try {
            Page<OperationInfo> page = new Page<>();
            QueryWrapper<OperationInfo> queryWrapper = new QueryWrapper<>(operationInfo);
            IPage<OperationInfo> result = iOperationInfoService.page(page, queryWrapper);
            return CommonResponse.ok(result);
        } catch (Exception e) {
            throw new SysCommonException("分页查询数据异常", e);
        }
    }

    @ApiOperation("新增数据")
    @PostMapping(value = "/save")
    public CommonResponse operationInfoSave(OperationInfo operationInfo) {
        try {
            operationInfo.setId(UuidUtil.randomUUID());
            iOperationInfoService.save(operationInfo);
            return CommonResponse.ok(operationInfo);
        } catch (Exception e) {
            throw new SysCommonException("新增数据异常", e);
        }
    }

    @ApiOperation("修改数据")
    @PutMapping(value = "/upd")
    public CommonResponse operationInfoUpdate(OperationInfo operationInfo) {
        try {
            iOperationInfoService.saveOrUpdate(operationInfo);
            return CommonResponse.ok(operationInfo);
        } catch (Exception e) {
            throw new SysCommonException("修改数据异常", e);
        }
    }

    @ApiOperation("根据id删除对象")
    @DeleteMapping(value = "del/{id}")
    public CommonResponse operationInfoDelete(@PathVariable String id) {
        try {
            int count = iOperationInfoService.removeById(id) ? 1 : 0;
            return CommonResponse.ok(count);
        } catch (Exception e) {
            throw new SysCommonException("根据id删除对象异常", e);
        }
    }

    @ApiOperation("批量删除对象")
    @DeleteMapping(value = "del/batch")
    public CommonResponse deleteBatchIds(@RequestParam List<Long> ids) {
        try {
            int count = iOperationInfoService.removeByIds(ids) ? 1 : 0;
            return CommonResponse.ok(count);
        } catch (Exception e) {
            throw new SysCommonException("批量删除对象异常", e);
        }
    }
}

