package com.base.server.modules.user.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.base.server.common.exception.SysCommonException;
import com.base.server.common.response.CommonResponse;
import com.base.server.common.utils.UuidUtil;
import com.base.server.modules.user.entity.UserGroupInfo;
import com.base.server.modules.user.service.UserGroupInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 用户组表 前端控制器
 * </p>
 * <p>
 * 作者: ostrich
 * 时间: 2019-11-28
 */
@Api(tags = "用户组表")
@RestController
@RequestMapping("api/rest/userGroupInfo")
public class UserGroupInfoController {
    private final Logger logger = LoggerFactory.getLogger(UserGroupInfoController.class);

    @Autowired
    public UserGroupInfoService iUserGroupInfoService;

    @ApiOperation("无分页查询数据")
    @GetMapping(value = "/list")
    public CommonResponse getUserGroupInfoList(UserGroupInfo userGroupInfo) {
        try {
            QueryWrapper<UserGroupInfo> queryWrapper = new QueryWrapper<>(userGroupInfo);
            List<UserGroupInfo> result = iUserGroupInfoService.list(queryWrapper);
            return CommonResponse.ok(result);
        } catch (Exception e) {
            throw new SysCommonException("无分页查询数据异常", e);
        }
    }

    @ApiOperation("分页查询数据")
    @GetMapping("/list/page")
    public CommonResponse getUserGroupInfoListPage(UserGroupInfo userGroupInfo,
                                                   @RequestParam(defaultValue = "1") Long currentPage,
                                                   @RequestParam(defaultValue = "10") Long pageSize) {
        try {
            Page<UserGroupInfo> page = new Page<>(currentPage, pageSize);
            IPage<UserGroupInfo> result = iUserGroupInfoService.selectByPage(page, userGroupInfo);
            return CommonResponse.ok(result);
        } catch (Exception e) {
            throw new SysCommonException("分页查询数据异常", e);
        }
    }

    @ApiOperation("新增数据")
    @PostMapping(value = "/save")
    public CommonResponse userGroupInfoSave(UserGroupInfo userGroupInfo) {
        try {
            userGroupInfo.setId(UuidUtil.randomUUID());
            userGroupInfo.setAddTime(LocalDateTime.now());
            iUserGroupInfoService.save(userGroupInfo);
            return CommonResponse.ok(userGroupInfo);
        } catch (Exception e) {
            throw new SysCommonException("新增数据异常", e);
        }
    }

    @ApiOperation("修改数据")
    @PutMapping(value = "/upd")
    public CommonResponse userGroupInfoUpdate(UserGroupInfo userGroupInfo) {
        try {
            userGroupInfo.setUpdateTime(LocalDateTime.now());
            iUserGroupInfoService.saveOrUpdate(userGroupInfo);
            return CommonResponse.ok(userGroupInfo);
        } catch (Exception e) {
            throw new SysCommonException("修改数据异常", e);
        }
    }

    @ApiOperation("根据id删除对象")
    @DeleteMapping(value = "del/{id}")
    public CommonResponse userGroupInfoDelete(@PathVariable String id) {
        try {
            int count = iUserGroupInfoService.removeById(id) ? 1 : 0;
            return CommonResponse.ok(count);
        } catch (Exception e) {
            throw new SysCommonException("根据id删除对象异常", e);
        }
    }

    @ApiOperation("批量删除对象")
    @DeleteMapping(value = "del/batch")
    public CommonResponse deleteBatchIds(@RequestParam List<Long> ids) {
        try {
            int count = iUserGroupInfoService.removeByIds(ids) ? 1 : 0;
            return CommonResponse.ok(count);
        } catch (Exception e) {
            throw new SysCommonException("批量删除对象异常", e);
        }
    }
}

