package com.base.server.modules.operate.service.impl;

import com.base.server.modules.operate.entity.OperationInfo;
import com.base.server.modules.operate.mapper.OperationInfoMapper;
import com.base.server.modules.operate.service.OperationInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 功能操作表 服务实现类
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
@Service
public class OperationInfoServiceImpl extends ServiceImpl<OperationInfoMapper, OperationInfo> implements OperationInfoService {

}
