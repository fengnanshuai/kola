package com.base.server.modules.user.service;

import com.base.server.modules.user.entity.UserGroupRelation;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户组与用户关联表 服务类
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
public interface UserGroupRelationService extends IService<UserGroupRelation> {

}
