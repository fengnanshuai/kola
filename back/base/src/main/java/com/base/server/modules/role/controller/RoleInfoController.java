package com.base.server.modules.role.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.base.server.common.exception.SysCommonException;
import com.base.server.common.response.CommonResponse;
import com.base.server.common.utils.UuidUtil;
import com.base.server.modules.role.entity.RoleInfo;
import com.base.server.modules.role.service.RoleInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 角色表 前端控制器
 * </p>
 * <p>
 * 作者: ostrich
 * 时间: 2019-11-28
 */
@Api(tags = "角色表")
@RestController
@RequestMapping("api/rest/roleInfo")
public class RoleInfoController {
    private final Logger logger = LoggerFactory.getLogger(RoleInfoController.class);

    @Autowired
    public RoleInfoService iRoleInfoService;

    @ApiOperation("无分页查询数据")
    @GetMapping(value = "/list")
    public CommonResponse getRoleInfoList(RoleInfo roleInfo) {
        try {
            QueryWrapper<RoleInfo> queryWrapper = new QueryWrapper<>(roleInfo);
            List<RoleInfo> result = iRoleInfoService.list(queryWrapper);
            return CommonResponse.ok(result);
        } catch (Exception e) {
            throw new SysCommonException("无分页查询数据异常", e);
        }
    }

    @ApiOperation("分页查询数据")
    @GetMapping("/list/page")
    public CommonResponse getRoleInfoListPage(RoleInfo roleInfo,
                                              @RequestParam(defaultValue = "1") Long currentPage,
                                              @RequestParam(defaultValue = "10") Long pageSize) {
        try {
            Page<RoleInfo> page = new Page<>(currentPage, pageSize);
            IPage<RoleInfo> result = iRoleInfoService.selectByPage(page, roleInfo);
            return CommonResponse.ok(result);
        } catch (Exception e) {
            throw new SysCommonException("分页查询数据异常", e);
        }
    }

    @ApiOperation("新增数据")
    @PostMapping(value = "/save")
    public CommonResponse roleInfoSave(RoleInfo roleInfo) {
        try {
            roleInfo.setId(UuidUtil.randomUUID());
            roleInfo.setAddTime(LocalDateTime.now());
            iRoleInfoService.save(roleInfo);
            return CommonResponse.ok(roleInfo);
        } catch (Exception e) {
            throw new SysCommonException("新增数据异常", e);
        }
    }

    @ApiOperation("修改数据")
    @PutMapping(value = "/upd")
    public CommonResponse roleInfoUpdate(RoleInfo roleInfo) {
        try {
            roleInfo.setUpdateTime(LocalDateTime.now());
            iRoleInfoService.saveOrUpdate(roleInfo);
            return CommonResponse.ok(roleInfo);
        } catch (Exception e) {
            throw new SysCommonException("修改数据异常", e);
        }
    }

    @ApiOperation("根据id删除对象")
    @DeleteMapping(value = "del/{id}")
    public CommonResponse roleInfoDelete(@PathVariable String id) {
        try {
            int count = iRoleInfoService.removeById(id) ? 1 : 0;
            return CommonResponse.ok(count);
        } catch (Exception e) {
            throw new SysCommonException("根据id删除对象异常", e);
        }
    }

    @ApiOperation("批量删除对象")
    @DeleteMapping(value = "del/batch")
    public CommonResponse deleteBatchIds(@RequestParam List<Long> ids) {
        try {
            int count = iRoleInfoService.removeByIds(ids) ? 1 : 0;
            return CommonResponse.ok(count);
        } catch (Exception e) {
            throw new SysCommonException("批量删除对象异常", e);
        }
    }
}

