package com.base.server.common.utils;


import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author hutongfu
 * @description: 组装菜单树
 * @since 2019/6/6 10:52
 */
public class BuildTree {

    public static <T> List<Tree<T>> buildList(List<Tree<T>> nodes, String idParam) {
        if (nodes == null) {
            return null;
        }
        List<Tree<T>> topNodes = new ArrayList<>();

        for (Tree<T> children : nodes) {
            String pid = children.getParentId();
            if (pid == null || idParam.equals(pid)) {
                topNodes.add(children);
                continue;
            }
            for (Tree<T> parent : nodes) {
                String id = parent.getId();
                if (id != null && id.equals(pid)) {
                    parent.getChildren().add(children);
                    children.setHasParent(true);
                    parent.setHasChildren(true);
                    continue;
                }
            }
        }
        return topNodes;
    }

    public static <T> List<Tree<T>> buildList(List<Tree<T>> nodes, String idParam, List<String> ids) {
        if (nodes == null) {
            return null;
        }
        List<Tree<T>> topNodes = new ArrayList<>();
        nodes.forEach(parent -> {
            if (Objects.equals(parent.getParentId(), idParam)) {
                parent.setChildren(getChild(parent.getId(), nodes, ids));
                if (ids.size() > 0 && ids.contains(parent.getId())) {
                    parent.setChecked(true);
                }
                //如果没有子级，则设置默认选中
                if (parent.getChildren() != null) {
                    parent.setHasChildren(true);
                }
                topNodes.add(parent);
            }
        });
        return topNodes;
    }

    public static <T> List<MenuTree<T>> buildMenuList(List<MenuTree<T>> nodes, String idParam) {
        if (nodes == null) {
            return null;
        }
        List<MenuTree<T>> topNodes = new ArrayList<>();
        nodes.forEach(children -> {
            String pid = children.getPId();
            if (pid == null || idParam.equals(pid)) {
                topNodes.add(children);
            }
            nodes.forEach(parent -> {
                String id = parent.getId();
                if (id != null && id.equals(pid)) {
                    parent.getChild().add(children);
                }
            });
        });

        return topNodes;
    }

    public static <T> List<Tree<T>> getChild(String pId, List<Tree<T>> originalData, List<String> ids) {

        // 子菜单
        List<Tree<T>> trees = new CopyOnWriteArrayList<>();
        //转换子级
        originalData.parallelStream().forEach(childObj -> {
            // 遍历所有节点，将父id与传过来的id比较
            if (!Objects.equals(childObj.getParentId(), "0") && Objects.equals(pId, childObj.getParentId())) {
                childObj.setHasParent(true);
                //设置是否默认选中
                if (ids != null && ids.size() != 0 && ids.contains(childObj.getId())) {
                    childObj.setChecked(true);
                }
                trees.add(childObj);
            }
        });

        // 把子菜单的子菜单再循环一遍
        trees.parallelStream().forEach(child -> {
            //递归
            child.setChildren(getChild(child.getId(), originalData, ids));
        });
        //排序
        trees.sort(Comparator.comparing(Tree::getOrder));
        return trees.size() > 0 ? trees : null;
    }
}