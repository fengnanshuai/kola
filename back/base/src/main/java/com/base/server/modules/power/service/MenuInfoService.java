package com.base.server.modules.power.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.base.server.common.response.CommonResponse;
import com.base.server.modules.power.entity.MenuInfo;

/**
 * <p>
 * 菜单表 服务类
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
public interface MenuInfoService extends IService<MenuInfo> {

    /**
     * 根据角色查询菜单
     *
     * @param param
     * @param type
     * @return
     */
    CommonResponse menuListByRole(MenuInfo param, String type);
}
