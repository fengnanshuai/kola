package com.base.server.modules.power.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.base.server.common.exception.SysCommonException;
import com.base.server.common.response.CommonResponse;
import com.base.server.common.utils.UuidUtil;
import com.base.server.modules.power.entity.MenuInfo;
import com.base.server.modules.power.service.MenuInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 菜单表 前端控制器
 * </p>
 * <p>
 * 作者: ostrich
 * 时间: 2019-11-28
 */
@Api(tags = "菜单表")
@RestController
@RequestMapping("api/rest/menuInfo")
public class MenuInfoController {
    private final Logger logger = LoggerFactory.getLogger(MenuInfoController.class);

    @Resource
    public MenuInfoService iMenuInfoService;

    @ApiOperation("无分页查询数据")
    @GetMapping(value = "/single")
    public CommonResponse getMenuInfo(MenuInfo menuInfo) {
        try {
            QueryWrapper<MenuInfo> query = Wrappers.query(menuInfo);
            MenuInfo one = iMenuInfoService.getOne(query);
            return CommonResponse.ok(one);
        } catch (Exception e) {
            throw new SysCommonException("无分页查询数据异常", e);
        }
    }

    @ApiOperation("无分页查询数据")
    @GetMapping(value = "/{type}")
    public CommonResponse getMenuInfoList(MenuInfo menuInfo, @PathVariable String type) {
        try {
            return iMenuInfoService.menuListByRole(menuInfo, type);
        } catch (Exception e) {
            throw new SysCommonException("无分页查询数据异常", e);
        }
    }

    @ApiOperation("分页查询数据")
    @GetMapping("/list/page")
    public CommonResponse getMenuInfoListPage(MenuInfo menuInfo) {
        try {
            Page<MenuInfo> page = new Page<>();
            QueryWrapper<MenuInfo> queryWrapper = new QueryWrapper<>(menuInfo);
            IPage<MenuInfo> result = iMenuInfoService.page(page, queryWrapper);
            return CommonResponse.ok(result);
        } catch (Exception e) {
            throw new SysCommonException("分页查询数据异常", e);
        }
    }

    @ApiOperation("新增数据")
    @PostMapping(value = "/save")
    public CommonResponse menuInfoSave(MenuInfo menuInfo) {
        try {
            menuInfo.setId(UuidUtil.randomUUID());
            menuInfo.setAddTime(LocalDateTime.now());
            if ("undefined".equals(menuInfo.getParentId()) || StringUtils.isBlank(menuInfo.getParentId())) {
                menuInfo.setParentId("0");
            }
            iMenuInfoService.save(menuInfo);
            return CommonResponse.ok(menuInfo);
        } catch (Exception e) {
            throw new SysCommonException("新增数据异常", e);
        }
    }

    @ApiOperation("修改数据")
    @PutMapping(value = "/upd")
    public CommonResponse menuInfoUpdate(MenuInfo menuInfo) {
        try {
            iMenuInfoService.saveOrUpdate(menuInfo);
            return CommonResponse.ok(menuInfo);
        } catch (Exception e) {
            throw new SysCommonException("修改数据异常", e);
        }
    }

    @ApiOperation("根据id删除对象")
    @DeleteMapping(value = "del/{id}")
    public CommonResponse menuInfoDelete(@PathVariable String id) {
        try {
            int count = iMenuInfoService.removeById(id) ? 1 : 0;
            return CommonResponse.ok(count);
        } catch (Exception e) {
            throw new SysCommonException("根据id删除对象异常", e);
        }
    }

    @ApiOperation("批量删除对象")
    @DeleteMapping(value = "del/batch")
    public CommonResponse deleteBatchIds(@RequestParam List<Long> ids) {
        try {
            int count = iMenuInfoService.removeByIds(ids) ? 1 : 0;
            return CommonResponse.ok(count);
        } catch (Exception e) {
            throw new SysCommonException("批量删除对象异常", e);
        }
    }
}

