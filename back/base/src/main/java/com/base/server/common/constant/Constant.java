package com.base.server.common.constant;

import java.awt.*;

/**
 * 描述：系统常量配置类
 * <p>
 * 作者：HuTongFu
 * 时间：2019/6/10 10:55
 */
public class Constant {

    /**
     * 返回状态值
     */
    public static class Result {
        //成功
        public static final int SUCCESS_CODE = 200;
        public static final String SUCCESS_MSG = "操作成功";
        //失败
        public static final int FAIL_CODE = 500;
        public static final String FAIL_MSG = "操作失败";
    }

    /**
     * 流程常量
     */
    public static class Flow {
        //流程引擎部署失败
        public static final int PROCESS_DEPLOY_FAILED = 20001;
        //流程挂起、激活、中止失败
        public static final int PROCESS_STATE_FAILED = 20002;
        //流程签收失败
        public static final int PROCESS_CLAIM_FAILED = 20003;
        //流程引擎同步数据失败
        public static final int PROCESS_SYN_FAILED = 20004;
        //流程定义命名空间
        public static final String PROCESS_NAME_SPACE = "http://b3mn.org/stencilset/bpmn2.0#";
        //开始节点
        public static final String START_EVENT = "startEvent";
        //结束节点
        public static final String END_EVENT = "endEvent";
        //用户任务节点
        public static final String USER_TASK = "userTask";
        //互斥网关
        public static final String EXCLUSIVE_GATEWAY = "exclusiveGateway";
        //并行网关
        public static final String PARALLEL_GATEWAY = "parallelGateway";
        //候选组
        public static final String CANDIDATE_GROUP = "candidateGroups";
        //候选人
        public static final String CANDIDATE_USER = "candidateUsers";
        //抄送人
        public static final String COPY_USER = "copyUser";
        //抄送组
        public static final String COPY_GROUP = "copyGroup";
        //服务任务节点
        public static final String SERVICE_TASK = "serviceTask";
        //顺序流
        public static final String SEQUENCE_FLOW = "sequenceFlow";
        //草稿
        public static final String TASK_DRAFT = "0";
        //同意
        public static final String TASK_AGREE = "1";
        //退回
        public static final String TASK_BACK = "2";
        //退回到发起节点
        public static final String TASK_BACK_2_START = "3";
        //挂起
        public static final String TASK_SUSPEND = "4";
        //激活
        public static final String TASK_ACTIVE = "5";
        //作废
        public static final String TASK_DELETE = "6";
        //完成
        public static final String TASK_FINISHED = "7";
        //任务创建监听类
        public static final String TASK_LISTENER_CREATE = "com.common.business.common.listener.process.TaskCreateListener";
        //动态流程图颜色定义
        public static final Color COLOR_COMMON = new Color(0, 0, 0);
        public static final Color COLOR_NORMAL = new Color(56, 187, 108);
        public static final Color COLOR_CURRENT = new Color(87, 132, 255);
        public static final Color COLOR_REBACK = new Color(239, 72, 138);
        public static final Color COLOR_SUSPPEND = new Color(130, 114, 250);
        public static final Color COLOR_CANCEL = new Color(167, 167, 167);
        //定义生成流程图时的边距(像素)
        public static final int PROCESS_PADDING = 5;
    }

    /**
     * 登录状态
     */
    public static class Permission {
        //未登录
        public static final int UN_LOGIN = 10001;
        //无权限
        public static final int NO_PERMISSION = 10002;
        //密码错误
        public static final int PWD_ERR = 10003;
        //登录成功
        public static final int SUCCESS = 10000;
    }

    /**
     * Jwt token 验证枚举值
     */
    public static class JWT {

        //账户
        public static final String ACCOUNT = "account";

        //时间戳
        public static final String CURRENT_TIME_MILLIS = "currentTimeMillis";

    }


    /**
     * RedisConstant 枚举值
     */
    public static class RedisConstant {
        //key(用于存储登录人的信息)
        public static final String PREFIX_TOKEN_U_CACHE = "kola:token:u:cache:";

        //key(用于刷新token不一致的问题)
        public static final String PREFIX_TOKEN_REFRESH_TIME = "kola:token:refresh:time:";

    }

    /**
     * 包扫描常量
     */
    public static class ScannerPackage {
        public static final String HANDLER_PACKAGE = "com.common.server.modules";
    }
}
