package com.base.server.modules.power.mapper;

import com.base.server.modules.power.entity.RolePermissionRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色与权限关联表 Mapper 接口
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
public interface RolePermissionRelationMapper extends BaseMapper<RolePermissionRelation> {

}
