package com.base.server.modules.notice.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.base.server.common.exception.SysCommonException;
import com.base.server.common.response.CommonResponse;
import com.base.server.common.utils.UuidUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;

import com.base.server.modules.notice.service.NoticeReceiverService;
import com.base.server.modules.notice.entity.NoticeReceiver;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;
/**
 * <p>
 * 消息收件人表 前端控制器
 * </p>
 *
 * 作者: ostrich
 * 时间: 2020-01-16
 */
@Api(tags = "消息收件人表")
@RestController
@RequestMapping("api/rest/noticeReceiver")
public class NoticeReceiverController {
    private final Logger logger=LoggerFactory.getLogger(NoticeReceiverController.class);

    @Autowired
    public NoticeReceiverService iNoticeReceiverService;

    @ApiOperation("无分页查询数据")
    @GetMapping(value = "/list")
    public CommonResponse getNoticeReceiverList(NoticeReceiver noticeReceiver) {
        try {
            QueryWrapper<NoticeReceiver> queryWrapper = new QueryWrapper<>(noticeReceiver);
            List<NoticeReceiver> result = iNoticeReceiverService.list(queryWrapper);
            return CommonResponse.ok(result);
        } catch (Exception e) {
            throw new SysCommonException("无分页查询数据异常", e);
        }
    }

    @ApiOperation("分页查询数据")
    @GetMapping("/list/page")
    public CommonResponse getNoticeReceiverListPage(NoticeReceiver noticeReceiver){
        try {
            Page<NoticeReceiver> page = new Page<>();
            QueryWrapper<NoticeReceiver> queryWrapper = new QueryWrapper<>(noticeReceiver);
            IPage<NoticeReceiver> result = iNoticeReceiverService.page(page, queryWrapper);
        return CommonResponse.ok(result);
        } catch (Exception e) {
            throw new SysCommonException("分页查询数据异常", e);
        }
    }

//    @ApiOperation("新增数据")
//    @PostMapping(value = "/save")
//    public CommonResponse noticeReceiverSave(NoticeReceiver noticeReceiver){
//        try{
//            noticeReceiver.setId(UuidUtil.randomUUID());
//            iNoticeReceiverService.save(noticeReceiver);
//            return CommonResponse.ok(noticeReceiver);
//        }catch(Exception e){
//            throw new SysCommonException("新增数据异常", e);
//        }
//    }

    @ApiOperation("修改数据")
    @PutMapping(value = "/upd")
    public CommonResponse noticeReceiverUpdate(NoticeReceiver noticeReceiver){
        try{
            iNoticeReceiverService.save(noticeReceiver);
            return CommonResponse.ok(noticeReceiver);
        }catch(Exception e){
            throw new SysCommonException("修改数据异常", e);
        }
    }

    @ApiOperation("根据id删除对象")
    @DeleteMapping(value = "del/{id}")
    public CommonResponse noticeReceiverDelete(@PathVariable String id){
        try{
            int count = iNoticeReceiverService.removeById(id) ? 1 : 0;
            return CommonResponse.ok(count);
        }catch(Exception e){
            throw new SysCommonException("根据id删除对象异常", e);
        }
    }

    @ApiOperation("批量删除对象")
    @DeleteMapping(value = "del/batch")
    public CommonResponse deleteBatchIds(@RequestParam List<Long> ids){
        try{
            int count = iNoticeReceiverService.removeByIds(ids)?1:0;
            return CommonResponse.ok(count);
        }catch(Exception e){
            throw new SysCommonException("批量删除对象异常", e);
        }
     }
}

