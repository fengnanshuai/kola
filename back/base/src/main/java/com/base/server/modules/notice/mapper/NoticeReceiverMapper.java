package com.base.server.modules.notice.mapper;

import com.base.server.modules.notice.entity.NoticeReceiver;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 消息收件人表 Mapper 接口
 * </p>
 *
 * @author ostrich
 * @since 2020-01-16
 */
public interface NoticeReceiverMapper extends BaseMapper<NoticeReceiver> {

}
