package com.base.server.modules.power.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 权限表
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
@Data
@ApiModel(value = "PermissionInfoVo", description = "权限表")
public class PermissionInfoVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    private String id;

    @ApiModelProperty(value = "权限类型")
    private String type;

    @ApiModelProperty(value = "权限名称")
    private String name;

    @ApiModelProperty(value = "权限名称")
    private String code;

    @ApiModelProperty(value = "权限描述")
    private String description;

    @ApiModelProperty(value = "添加时间")
    private LocalDateTime addTime;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "添加人")
    private String addUser;

    @ApiModelProperty(value = "修改人")
    private String updateUser;

    @ApiModelProperty(value = "状态")
    private Integer status;

}
